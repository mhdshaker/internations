internations
============
Please find ERD (ERD.svg) and Domain Model (Domain Model.svg) in root directory 

Discussion:

As the domain of the task was not mentioned we will consider it for a company (like Internations) where the company need admin portal for its employees with different roles to manage users joined different communities groups, and some more other functionalities.
Based on that, an employee need to have access to admin portal where he can manage users, and each employee should have email, password and role.
The roles that could be defined for such company could be vary from admin, country admin, groups admin to financial manger.



*Note 1:
ERD was designed with extra models for roles and resources management, but that was not implemented in this project as it is out of scope.

*Note 2:
The API is designed for public. Ideally it should be protected using OAuth2 and JWT.

*Note 3:
Frontend implementation is very basic as the task focusing more on backend.


*To run the project please follow the next steps:*

1- Install Project Dependancies 

composer install 
npm install
bower install

2- Create DB  

import user_managment.sql to mysql DB
 
3- Update DB connection  

update app/config/parameters.yml 

4- Init DB 

php app/console doctrine:fixtures:load --fixtures=src/AppBundle/DataFixtures


5- Run BDD Test

php bin/behat

6- Run WebApp server and Open browser 127.0.0.1:8000

php app/console server:start

7- API doc

http://127.0.0.1:8000/api/doc





