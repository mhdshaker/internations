<?php

use AppBundle\Exception\InvalidOperationException;
use AppBundle\Services\ServicesManager;
use Behat\Behat\Context\Context;

class ServiceContext implements Context
{
    /** @var AppBundle\Entity\Staff $admin */
    private $admin;
    /** @var  AppBundle\Entity\User $user */
    private $user;
    /** @var  AppBundle\Entity\Group $group */
    private $group;

    private $error;

    /** @var ServicesManager $servicesManager*/
    private $servicesManager;

    /** @var \AppBundle\Services\UserManager $userManager*/
    private $userManager;

    /** @var \AppBundle\Services\StaffManager $staffManager*/
    private $staffManager;

    /** @var \AppBundle\Services\GroupManager $groupManager*/
    private $groupManager;

    public function __construct(ServicesManager $servicesManager)
    {
        $this->servicesManager = $servicesManager;
        $this->userManager = $this->servicesManager->getServiceManager(\AppBundle\Services\UserManager::class);
        $this->staffManager = $this->servicesManager->getServiceManager(\AppBundle\Services\StaffManager::class);
        $this->groupManager = $this->servicesManager->getServiceManager(\AppBundle\Services\GroupManager::class);
    }


    /**
     * @Given I am an admin
     */
    public function iAmAnAdmin()
    {
        try{
            $this->admin = $this->staffManager->getAdmin();
        }catch (\Doctrine\ORM\EntityNotFoundException $e){
            $this->error = $e;
        }
    }

    /**
     * @When I add user :name
     * @param $name
     */
    public function iAddUser($name)
    {
        try{
            $this->user = $this->userManager->createUser($name);
        }catch (\InvalidArgumentException $e){
            $this->error = $e;
        }

    }

    /**
     * @Then I should get user his name :name
     * @param $name
     */
    public function iShouldGetUser($name)
    {
        assert($this->user instanceof AppBundle\Entity\User);
        assert($name === $this->user->getName());
    }


    /**
     * @When I add group :name
     * @param $name
     */
    public function iAddGroup($name)
    {
        try{
            $this->group = $this->groupManager->createGroup($name);
        }catch (\InvalidArgumentException $e){
            $this->error = $e;
        }
    }

    /**
     * @Then I should get group its name :name
     * @param $name
     */
    public function iShouldGetGroupItsNameDubai($name)
    {
        assert($this->group instanceof AppBundle\Entity\Group);
        assert($name === $this->group->getName());
    }

    /**
     * @Given I have empty group
     */
    public function iHaveEmptyGroup()
    {
        $this->group = $this->groupManager->createGroup('Empty Group');
    }

    /**
     * @When I delete group
     */
    public function iDeleteGroup()
    {
        try{
            $this->groupManager->deleteGroup($this->group);
        }catch (InvalidOperationException $e){
            $this->error = $e;
        }

    }

    /**
     * @Then group should be deleted
     */
    public function groupShouldBeDeleted()
    {
        assert($this->group instanceof AppBundle\Entity\Group && !$this->group->isActive());
    }

    /**
     * @Given I have non empty group
     */
    public function iHaveNonEmptyGroup()
    {
        $this->user = $this->userManager->createUser('Muhammad');
        $this->group = $this->groupManager->createGroup('Dubai');
        $this->groupManager->assignUserToGroup($this->user, $this->group, $this->admin);
    }

    /**
     * @Then group should not be deleted
     */
    public function groupShouldNotBeDeleted()
    {
        assert($this->group->isActive());
    }

    /**
     * @Given I have group :name
     * @param $name
     */
    public function iHaveGroup($name)
    {
        $this->group = $this->groupManager->createGroup($name);
    }

    /**
     * @Given I have user :name
     * @param $name
     */
    public function iHaveUser($name)
    {
        $this->user = $this->userManager->createUser($name);
    }


    /**
     * @When I add user to group
     */
    public function iAddUserToGroup()
    {
        try{
            $this->groupManager->assignUserToGroup($this->user, $this->group, $this->admin);
        }catch (InvalidOperationException $e){
            $this->error = $e;
        }

    }

    /**
     * @Then user should be part of group
     */
    public function userShouldBePartOfGroup()
    {
        assert($this->group->hasUser($this->user));
    }

    /**
     * @Given user is part of the group
     */
    public function userIsPartOfGroup()
    {
        $this->groupManager->assignUserToGroup($this->user,$this->group, $this->admin);
    }

    /**
     * @Then I should get error user is already part of this group
     */
    public function iShouldGetErrorUserIsAlreadyPartOfThisGroup()
    {
        assert($this->error instanceof InvalidOperationException and $this->error->getCode() == \AppBundle\Exception\ErrorLoader::ADD_MEMBER_TO_GROUP_ALREADY_PART_OF_IT);
    }

    /**
     * @When I delete user
     */
    public function iDeleteUser()
    {
        $this->userManager->deleteUser($this->user);
    }

    /**
     * @Then user should be removed
     */
    public function userShouldBeRemoved()
    {
        assert(!$this->user->isActive());
    }

    /**
     * @When I remove user from the group
     */
    public function iRemoveUserFromTheGroup()
    {
        try{
            $this->groupManager->removeUserFromGroup($this->user, $this->group, $this->admin);
        }catch (InvalidOperationException $e){
            $this->error = $e;
        }

    }

    /**
     * @Then user should not be part of group anymore
     */
    public function userShouldNotBePartOfGroup()
    {
        assert(!$this->group->hasUser($this->user));
    }

    /**
     * @Then I should get error user is not part of this group
     */
    public function iShouldGetErrorUserIsNotPartOfThisGroup()
    {
        assert($this->error instanceof InvalidOperationException and $this->error->getCode() == \AppBundle\Exception\ErrorLoader::REMOVE_MEMBER_FROM_GROUP_NOT_PART_OF_IT);
    }

}