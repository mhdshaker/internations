Feature: Add User to Group
  In order to organize users
  As an admin
  I need to assign users to group they are not already part of

  Scenario: Assign user to group he is not part of it
    Given I am an admin
    And I have empty group
    And I have user Muhammad
    When I add user to group
    Then user should be part of group


  Scenario: Assign user to group he is part of it
    Given I am an admin
    And I have group Dubai
    And I have user Muhammad
    And user is part of the group
    When I add user to group
    Then I should get error user is already part of this group