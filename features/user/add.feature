Feature: Add User
  In order to add user
  As an admin
  I need to provide name

  Scenario: Add user with name Muhammad
    Given I am an admin
    When I add user Muhammad
    Then I should get user his name Muhammad

