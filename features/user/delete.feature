Feature: Delete User
  In order to clean up users list
  As an admin
  I need to be able to delete users


  Scenario: Delete user
    Given I am an admin
    And I have user Muhammad
    When I delete user
    Then user should be removed

