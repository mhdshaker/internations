Feature: Remove User from Group
  In order to organize users
  As an admin
  I need to remove users from group they part of it

  Scenario: Remove user from group he is part of it
    Given I am an admin
    And I have empty group
    And I have user Muhammad
    And user is part of the group
    When I remove user from the group
    Then user should not be part of group anymore


  Scenario: Remove user from group he is not part of it
    Given I am an admin
    And I have empty group
    And I have user Muhammad
    When I remove user from the group
    Then I should get error user is not part of this group