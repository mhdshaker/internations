Feature: Delete group
  In order to clean up groups
  As an admin
  I need to be able to delete groups

  Scenario: Delete empty group
    Given I am an admin
    And I have empty group
    When I delete group
    Then group should be deleted

  Scenario: Delete non empty group
    Given I am an admin
    And I have non empty group
    When I delete group
    Then group should not be deleted
