Feature: Add Group
  In order to group user
  As an admin
  I need to provide name

  Scenario: Add group with name Dubai
    Given I am an admin
    When I add group Dubai
    Then I should get group its name Dubai

