<?php

namespace ApiBundle\Controller;


use AppBundle\Services\GroupManager;
use AppBundle\Services\StaffManager;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserGroupController extends ApiController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to assign user to group",
     *  requirements={
     *      {"name"="slug", "dataType"="integer", "required"=true, "description"="user id"},
     *      {"name"="id", "dataType"="string", "required"=true, "description"="group id"}
     *  }
     * )
     */
    // "post_user_group" [POST] /users/{slug}/groups/{id}
    public function postUserGroupAction($slug, $id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($slug);

        $group = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->find($id);

        $staff = $this->get('app_bundle.services.services_manager')->getServiceManager(StaffManager::class)->getAdmin();

        /** @var GroupManager $groupManager */
        $groupManager = $this->get('app_bundle.services.services_manager')->getServiceManager(GroupManager::class);
        return $groupManager->assignUserToGroup($user, $group, $staff);
    }

    // "delete_user_group" [DELETE] /users/{slug}/groups/{id}
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to remove user from group",
     *  requirements={
     *      {"name"="slug", "dataType"="integer", "required"=true, "description"="user id"},
     *      {"name"="id", "dataType"="string", "required"=true, "description"="group id"}
     *  }
     * )
     */
    public function deleteUserGroupAction($slug, $id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($slug);

        $group = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->find($id);

        $staff = $this->get('app_bundle.services.services_manager')->getServiceManager(StaffManager::class)->getAdmin();

        /** @var GroupManager $groupManager */
        $groupManager = $this->get('app_bundle.services.services_manager')->getServiceManager(GroupManager::class);
        return $groupManager->removeUserFromGroup($user, $group, $staff);
    }

}