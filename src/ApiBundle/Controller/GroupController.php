<?php

namespace ApiBundle\Controller;

use AppBundle\Services\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class GroupController extends ApiController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to search groups by name",
     *  parameters={
     *      {"name"="query", "dataType"="string", "required"=true, "description"="Name of group"}
     *  }
     * )
     */
    public function getGroupsSearchAction(Request $request){
        $query = $request->query->get('query');
        $groups = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->searchFilter('name', $query)
            ->getResult()
        ;

        return ['result' => $groups];
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to list groups",
     * )
     */
    public function getGroupsAction()
    {
        $groups = $this->getDoctrine()
            ->getRepository('AppBundle:Group')->getResult()
        ;


        return ['result' => $groups];
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to get group by ID",
     *  requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="Group ID"}
     *  }
     * )
     */
    public function getGroupAction($id)
    {
        $group = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);

        if (is_null($group)) {
            throw $this->createNotFoundException('No such article');
        }
        return ['result' => $group];
    }


    // "post_group" [POST] /groups
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to create group",
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="Group Name"}
     *  }
     * )
     */
    public function postGroupAction(Request $request)
    {
        $name = $request->get('name');
        $groupManager = $this->get('app_bundle.services.services_manager')->getServiceManager(GroupManager::class);
        $group = $groupManager->createGroup($name);
        return $group;
    }

    // "delete_group" [DELETE] /groups/{id}
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to delete group",
     *  requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="Group ID"}
     *  }
     * )
     */
    public function deleteGroupAction($id)
    {
        $group = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->find($id);

        $groupManager = $this->get('app_bundle.services.services_manager')->getServiceManager(GroupManager::class);
        $user = $groupManager->deleteGroup($group);
        return $user;

    }

}