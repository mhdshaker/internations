<?php
namespace ApiBundle\Controller;
use AppBundle\Services\UserManager;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class UserController extends ApiController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to return list of users",
     * )
     */
    public function getUsersAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')->getResult()
            ;

        return ['result' => $users];
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to return a user by ID",
     *  requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="user id"}
     *  }
     * )
     */
    public function getUserAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);

        if (is_null($user)) {
            throw $this->createNotFoundException('No such article');
        }
        return ['result' => $user];
    }

    // "post_user" [POST] /users
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to create user",
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="user name"}
     *  }
     * )
     */
    public function postUserAction(Request $request)
    {
        $name = $request->get('name');
        $userManager = $this->get('app_bundle.services.services_manager')->getServiceManager(UserManager::class);
        $user = $userManager->createUser($name);
        return $user;
    }

    // "delete_user" [DELETE] /users/{id}
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to delete user",
     *  requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="User ID"}
     *  }
     * )
     */
    public function deleteUserAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);

        $userManager = $this->get('app_bundle.services.services_manager')->getServiceManager(UserManager::class);
        $user = $userManager->deleteUser($user);
        return $user;
    }

}