<?php

namespace AppBundle\Exception;


abstract class AppBundleException extends \Exception
{

    public $params = array();
    static public function throwByErrorCode($code, $complementaryMessage = "", $params = []){
        $class = get_called_class();
        $exception = new $class(ErrorLoader::GetErrorMessage($code, $complementaryMessage , 'en', $params), $code);
        $exception->params = $params;
        throw $exception;
    }

}