<?php
namespace AppBundle\Exception;

class ErrorLoader
{
    // 10xx General errors
    // 11xx Group errors
    // 12xx Staff errors

    const GENERAL_ERROR                                     = 1000;
    const INVALID_PARAMS                                    = 1001;
    const ACCESS_DENIED_ERROR                               = 1002;

    const GROUP_HAS_MEMBERS_CAN_NOT_BE_DELETED_ERROR        = 1101;
    const INACTIVE_GROUP_CAN_NOT_BE_EDITED_ERROR            = 1102;
    const ADD_MEMBER_TO_GROUP_ALREADY_PART_OF_IT            = 1103;
    const REMOVE_MEMBER_FROM_GROUP_NOT_PART_OF_IT           = 1104;

    const STAFF_EMAIL_TAKEN                                 = 1201;

    static protected $Errors = [
        self::GENERAL_ERROR                                 =>  [
            "en"    =>  "Oops! An Error Occurred.\n We are sorry for any inconvenience caused.",
            "de"    =>  "Oops! An Error Occurred.\n We are sorry for any inconvenience caused."
        ],

        self::INVALID_PARAMS                                =>  [
            "en"    =>  "Invalid Request.",
            "de"    =>  "Invalid Request."
        ],

        self::ACCESS_DENIED_ERROR                           =>  [
            "en"    =>  "Access Denied!\n You don't have the permission to access this resource.",
            "de"    =>  "Access Denied!\n You don't have the permission to access this resource."
        ],

        self::GROUP_HAS_MEMBERS_CAN_NOT_BE_DELETED_ERROR    => [
            "en"    =>  "Group can not be deleted!.\n The group you are trying to delete has #members_count# member(s).",
            "de"    =>  "Group can not be deleted!.\n The group you are trying to delete has #members_count# member(s).",
        ],

        self::INACTIVE_GROUP_CAN_NOT_BE_EDITED_ERROR        => [
            "en"    =>  "Member can not be added!.\n The group you are trying to edit has been deactivated.",
            "de"    =>  "Member can not be added!.\n The group you are trying to edit has been deactivated.",
        ],

        self::ADD_MEMBER_TO_GROUP_ALREADY_PART_OF_IT        => [
            "en"    =>  "Member already joined!.",
            "de"    =>  "Member already joined!",
        ],

        self::REMOVE_MEMBER_FROM_GROUP_NOT_PART_OF_IT        => [
            "en"    =>  "Member is not part of this group!.",
            "de"    =>  "Member is not part of this group!",
        ],


        self::STAFF_EMAIL_TAKEN                              => [
            "en"    =>  "Staff Can not be added.\n Email is taken",
            "de"    =>  "Staff Can not be added.\n Email is taken"
        ]

    ];

    static private function parametarizedMessage($message, $params){
        foreach($params as $key => $value){
            $message = str_replace("#{$key}#", $value, $message);
        }

        return $message;
    }

    static public function GetErrorMessage($number, $ComplementaryMessage = "", $lang = "en", $params = []) {
        $number = (int) $number;
        switch ($number) { //To set special handling of errors
            default:
                break;
        }

        if (array_key_exists($number, self::$Errors)) {
            if($lang == false){
                $ErrorMessage = self::$Errors[$number];
            }else{
                $ErrorMessage = self::$Errors[$number][$lang];
                if ($ComplementaryMessage)
                    $ErrorMessage.= " ".$ComplementaryMessage;
            }

        }else{
            $ErrorMessage = self::$Errors[self::GENERAL_ERROR][$lang];
        }


        return self::parametarizedMessage($ErrorMessage, $params);
    }
}