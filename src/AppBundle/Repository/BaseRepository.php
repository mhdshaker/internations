<?php
namespace AppBundle\Repository;

use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;

class BaseRepository extends EntityRepository
{

    /** @var  Criteria */
    protected $criteria;

    /** @var  QueryBuilder */
    protected $qb;


    /** @var array holds current joins added to query builder */
    protected $joins = array();

    /**
     * Get an alias for entity name.
     * @param string $entityName
     * @return string
     */
    protected function getAlias($entityName)
    {
        $path = explode('\\', $entityName);
        $alias = array_pop($path);
        return "_{$alias}";
    }

    /**
     * Adds an inner join to Query Builder.
     *
     * @param $joinEntity
     * @param null $condition
     * @param null $alias
     * @return BaseRepository
     */
    protected function addJoin($joinEntity, $condition = null, $alias = null)
    {
        return $this->addJoinToQB($joinEntity, Join::INNER_JOIN, $condition, $alias);
    }

    /**
     * Adds a left join to Query Builder.
     *
     * @param $joinEntity
     * @param null $condition
     * @param null $alias
     * @return BaseRepository
     */
    protected function addLeftJoin($joinEntity, $condition = null, $alias = null)
    {
        return $this->addJoinToQB($joinEntity, Join::LEFT_JOIN, $condition, $alias);
    }

    /**
     * Adds a join statement to Query Builder.
     *
     * @param $joinEntity
     * @param $joinType
     * @param null $condition
     * @param null $alias
     * @return $this
     */
    protected function addJoinToQB($joinEntity, $joinType, $condition = null, $alias = null)
    {
        $alias = $alias ?: $this->getAlias($joinEntity);
        if (array_key_exists($alias, $this->joins))
            return $this;

        $conditionType = $condition ? Join::WITH : null;

        $qb = $this->getQueryBuilder();
        if ($joinType === Join::INNER_JOIN)
            $qb->innerJoin($joinEntity, $alias, $conditionType, $condition);
        else
            $qb->leftJoin($joinEntity, $alias, $conditionType, $condition);

        $this->joins [$alias] = $joinEntity;
        return $this;
    }


    /**
     * Gets criteria object.
     * @return Criteria
     */
    public function getCriteria()
    {
        if (is_null($this->criteria))
            $this->criteria = Criteria::create();

        return $this->criteria;
    }

    /**
     * Gets query builder object.
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        if (is_null($this->qb))
            $this->initQueryBuilder();

        return $this->qb;
    }

    public function getQuery()
    {
        return $this->getQueryBuilder()->getQuery();
    }

    public function getSingleScalarResult()
    {
        return $this->getQuery()->getSingleScalarResult();
    }


    public function initQueryBuilder()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $alias = $this->getEntityAlias();

        $qb->select($alias);
        $qb->from($this->getEntityName(), $alias);

        $this->qb = $qb;
    }

    public function getEntityAlias()
    {
        return $this->getAlias($this->getEntityName());
    }


    /**
     * Add where expression
     *
     * @param $expression
     * @return $this
     */
    public function addExpression($expression)
    {
        $this->getCriteria()->andWhere($expression);

        return $this;
    }

    /**
     * Add simple equality expression.
     *
     * @param $field string field name
     * @param $value string field value
     * @return $this
     */
    public function addFilter($field, $value)
    {
        if(is_array($value))
            return $this->addExpression(Criteria::expr()->in($field, $value));

        return $this->addExpression(Criteria::expr()->eq($field, $value));
    }

    public function searchFilter($field, $value)
    {
        return $this->addExpression(Criteria::expr()->contains($field, $value));
    }

    /**
     * Return results matching to current criteria.
     *
     * @return array
     */
    public function getResult()
    {
        $this->getQueryBuilder()->addCriteria($this->getCriteria());
        $q = $this->getQueryBuilder()->getQuery();
        $sql = $q->getSQL();
        $result = $this->getQueryBuilder()->getQuery()->getResult();

        $this->reset();

        return $result;
    }

    /**
     * Clear saved query params.
     */
    public function reset()
    {
        // clear criteria
        $this->criteria = null;
        // clear joins
        $this->joins = array();
        // clear qb
        $this->qb = null;
    }

    public function getRawSql()
    {
        $this->getQueryBuilder()->addCriteria($this->getCriteria());
        $q = $this->getQueryBuilder()->getQuery();
        return $q->getSQL();
    }

    /**
     * get first matching result.
     *
     * @return BaseEntity
     */
    public function getFirstResult()
    {
        $this->getQueryBuilder()->addCriteria($this->getCriteria());
        $q = $this->getQueryBuilder()->getQuery();
        $sql = $q->getSQL();
        $result = $this->getQueryBuilder()->getQuery()->getResult();

        $this->reset();

        if(count($result) > 0)
            return $result[0];

        return null;
    }

    /**
     * Filter results that are active only.
     * Here we assume '1' is the active state, otherwise it should be overridden.
     *
     * @return $this
     */
    public function active()
    {
        return $this->addFilter('status', BaseEntity::STATUS_ACTIVE);
    }

    /**
     * Set select statement in query builder.
     *
     * @param $select
     * @return $this
     */
    public function select($select)
    {
        $this->getQueryBuilder()->select($select);

        return $this;
    }

    /**
     * Add a select statement in query builder.
     *
     * @param $select
     * @return $this
     */
    public function addSelect($select)
    {
        $this->getQueryBuilder()->addSelect($select);

        return $this;
    }
}