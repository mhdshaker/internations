<?php

namespace AppBundle\Repository;


class UserGroupRepository extends BaseRepository
{
    /**
     * @param $user
     * @return $this
     */
    public function hasUser($user)
    {
        if (is_null($user))
            return $this;
        return $this->addFilter('user', $user);
    }

    public function hasGroup($group)
    {
        if (is_null($group))
            return $this;
        return $this->addFilter('group', $group);
    }

}