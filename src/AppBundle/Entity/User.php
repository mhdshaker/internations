<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends Person
{

    public function __construct($firstName, $lastName=null, $gender=Person::GENDER_UNKNOWN){

        parent::__construct($firstName, $lastName=null, $gender=Person::GENDER_UNKNOWN);
        $this->userGroups = new ArrayCollection();
    }

    public function getDiscr()
    {
        return 'user';
    }

    /**
     *
     * @ORM\OneToMany(targetEntity="UserGroup", mappedBy="user")
     */
    private $userGroups;


    /**
     * @return ArrayCollection
     */
    public function getUserGroups(){
        return $this->userGroups;
    }

    /**
     * @param UserGroup $userGroup
     */
    public function addUserGroup($userGroup){
        if($userGroup instanceof UserGroup)
            $this->userGroups->add($userGroup);
    }

    /**
     * @param UserGroup $userGroup
     */
    public function removeUserGroup($userGroup){
        if($userGroup instanceof UserGroup)
            $this->userGroups->remove($userGroup);
    }

    public function getGroups()
    {
        $groups = [];
        foreach ($this->userGroups as $userGroup)
            array_push($groups, $userGroup->getGroup());
        return $groups;
    }

}
