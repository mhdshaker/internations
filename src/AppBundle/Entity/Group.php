<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Group
 *
 * @ORM\Table(name="`group`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Group extends BaseEntity
{
    public function __construct(){
        $this->groupUsers = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = BaseEntity::STATUS_ACTIVE;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     *
     * @ORM\OneToMany(targetEntity="UserGroup", mappedBy="group")
     */
    private $groupUsers;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Group
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusText(){
        return BaseEntity::StatusText($this->status);
    }

    public function deactivate()
    {
        $this->status = self::STATUS_INACTIVE;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }
    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Group
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroupUsers(){
        return $this->groupUsers;
    }

    /**
     * @param UserGroup $userGroup
     */
    public function addUserGroup($userGroup){
        if($userGroup instanceof UserGroup)
            $this->groupUsers->add($userGroup);
    }

    /**
     * @param UserGroup $userGroup
     */
    public function removeUserGroup($userGroup){
        if($userGroup instanceof UserGroup)
            $this->groupUsers->remove($userGroup);
    }


    public function getUsers()
    {
        $users = [];
        foreach ($this->groupUsers as $groupUser)
            array_push($users, $groupUser->getUser());
        return $users;
    }

    public function getActiveUsers()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("status", BaseEntity::STATUS_ACTIVE));

        $activeGroupUsers = $this->groupUsers->matching($criteria);
        $activeUsers = $activeGroupUsers->filter(function($activeGroupUser){
            /** @var UserGroup $activeGroupUser  */
            return $activeGroupUser->getUser()->getStatus() == BaseEntity::STATUS_ACTIVE?:$activeGroupUser->getUser();
        });


        return $activeUsers;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function hasUser($user){
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("status", BaseEntity::STATUS_ACTIVE))
            ->andWhere(Criteria::expr()->eq("user", $user));

        return $this->groupUsers->matching($criteria)->count()>0;
    }

    public function isEmpty()
    {
        return $this->getNumOfUsers()==0;
    }

    public function getNumOfUsers()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("status", BaseEntity::STATUS_ACTIVE));

        return $this->groupUsers->matching($criteria)->count();
    }

    /**
     * @ORM\PrePersist
     */
    public function doOnPrePersist()
    {
        $this->dateCreated = new \DateTime();
    }
}
