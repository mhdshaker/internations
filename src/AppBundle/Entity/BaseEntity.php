<?php
namespace AppBundle\Entity;


abstract class BaseEntity
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = -1;


    static protected function StatusText($status){
        return $status==self::STATUS_ACTIVE?"Active":"Deactivated";
    }

}