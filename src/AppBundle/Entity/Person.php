<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

use JMS\Serializer\Annotation\Expose;

//* @ORM\DiscriminatorColumn(name="discr", type="string")
//* @ORM\DiscriminatorMap({"staff" = "Staff", "user" : "User"})
//* @ORM\DiscriminatorMap({"staff" = "AppBundle\Entity\Staff", "user" : "AppBundle\Entity\User"})

//* @JMS\Discriminator(field="discr", map={"staff": "AppBundle\Entity\Staff", "user": "AppBundle\Entity\User"})
/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"staff" = "AppBundle\Entity\Staff", "user" : "AppBundle\Entity\User"})

 * @ORM\HasLifecycleCallbacks
 */
abstract class Person extends BaseEntity
{
    const GENDER_UNKNOWN = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;



    abstract public function getDiscr();

    public function __construct($firstName, $lastName=null, $gender=Person::GENDER_UNKNOWN){

        $this->setFirstName($firstName)
            ->setLastName($lastName)
            ->setGender($gender);
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=32, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=32, nullable=true)
     */
    private $lastName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="gender", type="integer", nullable=false)
     */
    private $gender;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = self::STATUS_ACTIVE;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set gender
     *
     * @param boolean $gender
     * @return Person
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return boolean 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Person
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusText(){
        return BaseEntity::StatusText($this->status);
    }

    public function isActive(){
        return $this->status == BaseEntity::STATUS_ACTIVE;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Person
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @ORM\PrePersist
     */
    public function doOnPrePersist()
    {
        $this->dateCreated = new \DateTime();
    }

    public function deactivate()
    {
        $this->status = self::STATUS_INACTIVE;
    }

    public function getName(){
        if($this->getLastName() == null)
            return $this->getFirstName();
        return "{$this->getFirstName()} {$this->getLastName()}";
    }

}
