<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserGroup
 *
 * @ORM\Table(name="user_group", indexes={@ORM\Index(name="group_id", columns={"group_id"}), @ORM\Index(name="IDX_8F02BF9DA76ED395", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserGroupRepository")
 * @ORM\HasLifecycleCallbacks
 */
class UserGroup extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = BaseEntity::STATUS_ACTIVE;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     * @var User
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var Group
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Group")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     * })
     */
    private $group;

    /**
     * @var Staff
     * @ORM\ManyToOne(targetEntity="Staff")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="added_by_staff_id", referencedColumnName="id")
     * })
     */
    private $addedByStaff;


    /**
     * Set status
     *
     * @param integer $status
     * @return UserGroup
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function isActive()
    {
        return $this->status == BaseEntity::STATUS_ACTIVE;
    }
    public function getStatusText(){
        return BaseEntity::StatusText($this->status);
    }

    public function deactivate()
    {
        $this->status = BaseEntity::STATUS_INACTIVE;
    }

    public function activate()
    {
        $this->status = BaseEntity::STATUS_ACTIVE;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return UserGroup
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserGroup
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set staff
     *
     * @param \AppBundle\Entity\Staff $staff
     * @return UserGroup
     */
    public function setAddedByStaff(\AppBundle\Entity\Staff $staff)
    {
        $this->addedByStaff = $staff;

        return $this;
    }

    /**
     * Get staff
     *
     * @return \AppBundle\Entity\Staff
     */
    public function getAddedByStaff()
    {
        return $this->addedByStaff;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     * @return UserGroup
     */
    public function setGroup(\AppBundle\Entity\Group $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @ORM\PrePersist
     */
    public function doOnPrePersist()
    {
        $this->dateCreated = new \DateTime();
    }
}
