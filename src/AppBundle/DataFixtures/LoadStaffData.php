<?php

use AppBundle\Entity\Person;
use AppBundle\Entity\Staff;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadStaffData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $staff = $this->container->get('app_bundle.services.staff_manager')
            ->createStaff('ira@internations.org', 'password','Ira');

        $this->addReference('added-by-staff', $staff);
    }

    public function getOrder(){
        return 1;
    }
}