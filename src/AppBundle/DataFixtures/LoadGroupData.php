<?php


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Group;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $group = new Group();
        $group->setName('Dubai');

        $manager->persist($group);
        $manager->flush();

        $this->addReference('dubai-group', $group);
    }

    public function getOrder(){
        return 2;
    }
}