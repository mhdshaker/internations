<?php

use AppBundle\Entity\UserGroup;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $user_group = new UserGroup();
        $user_group->setUser($this->getReference('user'))
            ->setGroup($this->getReference('dubai-group'))
            ->setAddedByStaff($this->getReference('added-by-staff'));

        $manager->persist($user_group);
        $manager->flush();
    }

    public function getOrder(){
        return 4;
    }
}