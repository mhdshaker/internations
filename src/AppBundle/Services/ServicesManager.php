<?php
namespace AppBundle\Services;


use AppBundle\Repository\BaseRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class ServicesManager
{
    /** @var array */
    private $dependencies;

    /** @var  EntityManager $em */
    protected $entityManager;
    /** @var BaseRepository */
    protected $repository;

    protected $encoderFactory;

    protected function initRepository($entityManager){
        $pathComponents = explode('\\', get_class($this));
        $serviceName = array_pop($pathComponents);
        $pos = strrpos($serviceName, 'Manager');
        if ($pos !== false)
            $entityName = substr_replace($serviceName, '', $pos, strlen('Manager'));
        try {
            $this->repository = $entityManager->getRepository("AppBundle:$entityName");
        } catch (\Exception $e) {
            /** Not all services will have repository
             *  Only the ones managing entity repository will have one
             */
        }
    }

    public function __construct(EntityManager $entityManager, EncoderFactoryInterface $encoderFactory=null)
    {
        $this->entityManager = $entityManager;
        $this->dependencies = array();
        $this->encoderFactory = $encoderFactory;
        $this->initRepository($entityManager);
    }


    public function getServiceManager($serviceManager){
        if (array_key_exists($serviceManager, $this->dependencies)) {
            return $this->dependencies[$serviceManager];
        }

        $serviceManagerInstance = new $serviceManager($this->entityManager);
        $this->dependencies[$serviceManager] = $serviceManagerInstance;
        return $serviceManagerInstance;
    }
}