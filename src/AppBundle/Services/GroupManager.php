<?php

namespace AppBundle\Services;


use AppBundle\Entity\Group;
use AppBundle\Entity\Staff;
use AppBundle\Entity\User;
use AppBundle\Entity\UserGroup;
use AppBundle\Exception\AccessDeniedException;
use AppBundle\Exception\InvalidOperationException;
use AppBundle\Exception\ErrorLoader;
use AppBundle\Repository\UserGroupRepository;
use Doctrine\ORM\EntityNotFoundException;
use AppBundle\Exception\InvalidArgumentException;


class GroupManager extends ServicesManager
{
    /**
     * @param $name
     * @return Group
     * @throws InvalidArgumentException
     */
    public function createGroup($name)
    {
        if(!is_string($name))
            InvalidArgumentException::throwByErrorCode(ErrorLoader::INVALID_PARAMS);

        $group = new Group();
        $group->setName($name);
        $this->entityManager->persist($group);
        $this->entityManager->flush($group);

        return $group;
    }

    /**
     * @param Group $group
     * @return Group
     * @throws InvalidArgumentException
     */
    public function deleteGroup($group)
    {
        if(!$group instanceof Group)
            InvalidArgumentException::throwByErrorCode(ErrorLoader::INVALID_PARAMS);

        if(!$group->isEmpty())
            InvalidOperationException::throwByErrorCode(ErrorLoader::GROUP_HAS_MEMBERS_CAN_NOT_BE_DELETED_ERROR,'',['members_count'=>$group->getNumOfUsers()]);

        $group->deactivate();

        $this->entityManager->persist($group);
        $this->entityManager->flush($group);

        return $group;
    }


    /**
     * @param User $user
     * @param Group $group
     * @param Staff $staff
     * @return Group
     */
    public function assignUserToGroup($user, $group, $staff)
    {

        if(!$user instanceof User  || !$group instanceof Group || !$staff instanceof Staff)
            InvalidArgumentException::throwByErrorCode(ErrorLoader::INVALID_PARAMS);

        /** for simplicity we are considering all staff has admin role */
        if(!$staff->hasRole())
            AccessDeniedException::throwByErrorCode(ErrorLoader::ACCESS_DENIED_ERROR);

        if(!$group->isActive())
            InvalidOperationException::throwByErrorCode(ErrorLoader::INACTIVE_GROUP_CAN_NOT_BE_EDITED_ERROR);

        if($group->hasUser($user))
            InvalidOperationException::throwByErrorCode(ErrorLoader::ADD_MEMBER_TO_GROUP_ALREADY_PART_OF_IT);

        $this->createUserGroup($group, $user, $staff);
        return $group;
    }


    /**
     * @param User $user
     * @param Group $group
     * @param Staff $staff
     * @return Group
     */
    public function removeUserFromGroup($user, $group, $staff)
    {

        if(!$user instanceof User  || !$group instanceof Group || !$staff instanceof Staff)
            InvalidArgumentException::throwByErrorCode(ErrorLoader::INVALID_PARAMS);

        /** for simplicity we are considering all staff has admin role */
        if(!$staff->hasRole())
            AccessDeniedException::throwByErrorCode(ErrorLoader::ACCESS_DENIED_ERROR);

        if(!$group->isActive())
            InvalidOperationException::throwByErrorCode(ErrorLoader::INACTIVE_GROUP_CAN_NOT_BE_EDITED_ERROR);

        if(!$group->hasUser($user))
            InvalidOperationException::throwByErrorCode(ErrorLoader::REMOVE_MEMBER_FROM_GROUP_NOT_PART_OF_IT);

        $this->removeUserGroup($group, $user, $staff);
        return $group;
    }

    /**
     * @param User $user
     * @param Group $group
     * @param Staff $staff
     * @return UserGroup
     */
    private function createUserGroup($group, $user, $staff)
    {
        /** @var UserGroupRepository $repo */
        $repo = $this->entityManager->getRepository('AppBundle:UserGroup');
        /** @var UserGroup $userGroup */
        $userGroup = $repo->hasUser($user)
            ->hasGroup($group)->getFirstResult();

        if($userGroup){
            $userGroup->activate();
        }else{
            $userGroup = new UserGroup();
            $userGroup->setGroup($group)
                ->setUser($user)
                ->setAddedByStaff($staff);

            $group->addUserGroup($userGroup);
            $user->addUserGroup($userGroup);
        }

        $this->entityManager->persist($userGroup);
        $this->entityManager->flush($userGroup);
        return $userGroup;
    }


    /**
     * @param Group $group
     * @param User $user
     * @param Staff $staff
     * @throws EntityNotFoundException
     */
    private function removeUserGroup($group, $user, $staff)
    {
        /** @var UserGroupRepository $repo */
        $repo = $this->entityManager->getRepository('AppBundle:UserGroup');
        /** @var UserGroup $userGroup */
        $userGroup = $repo->hasUser($user)
            ->hasGroup($group)
            ->getFirstResult();

        if(!$userGroup)
            throw new EntityNotFoundException();

        $userGroup->deactivate();

        $this->entityManager->persist($userGroup);
        $this->entityManager->flush($userGroup);

    }

}