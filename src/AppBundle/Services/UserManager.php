<?php


namespace AppBundle\Services;


use AppBundle\Entity\User;
use AppBundle\Exception\ErrorLoader;
use AppBundle\Exception\InvalidArgumentException;
use Doctrine\ORM\Query\AST\Join;

class UserManager extends ServicesManager
{
    /**
     * @param $name
     * @return User
     * @throws InvalidArgumentException
     */
    public function createUser($name)
    {
        if(!is_string($name) || strlen($name) == 0)
            InvalidArgumentException::throwByErrorCode(ErrorLoader::INVALID_PARAMS);

        $user = new User($name);
        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function deleteUser($user)
    {
        if(!$user instanceof User)
            InvalidArgumentException::throwByErrorCode(ErrorLoader::INVALID_PARAMS);

        $user->deactivate();
        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        return $user;
    }


}