<?php


namespace AppBundle\Services;

use AppBundle\Exception\ErrorLoader;
use AppBundle\Exception\LogicException;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Person;
use AppBundle\Entity\Staff;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class StaffManager extends ServicesManager implements UserProviderInterface
{

    public function createStaff($email, $plainPassword, $firstName, $lastName=null, $gender=Person::GENDER_UNKNOWN)
    {
//        $email = 'value_to_validate';
//        // ...
//
//        $emailConstraint = new EmailConstraint();
//        $emailConstraint->message = 'Your customized error message';
//
//        $errors = $this->get('validator')->validateValue(
//            $email,
//            $emailConstraint
//        );


        $email = strtolower($email);
        $staff = $this->repository->addFilter('email',$email)->getFirstResult();
        if($staff)
            LogicException::throwByErrorCode(ErrorLoader::STAFF_EMAIL_TAKEN);



        $staff = new Staff($firstName, $lastName, $gender);
        $password = $this->encoderFactory->getEncoder($staff)->encodePassword($plainPassword, null);
        $staff->setEmail($email);
        $staff->setPassword($password);

        $this->entityManager->persist($staff);
        $this->entityManager->flush();

        return $staff;
    }
    /**
     * Here we consider all Staffs are admins for simplicity
     * For real life implementation we should filter staff who has role admin
     * @return \AppBundle\Entity\Staff
     * @throws EntityNotFoundException
     */
    public function getAdmin()
    {
        /** @var Staff $admin */
        $admin = $this->repository->getFirstResult();
        if (!$admin){
            throw new EntityNotFoundException();
        }

        return $admin;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @see UsernameNotFoundException
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $staff = $this->repository->addFilter('email', strtolower($username))->getFirstResult();
        if(!$staff)
            throw new UsernameNotFoundException();
        return $staff;
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        /** @var Staff $staff */
        $staff = $this->repository->find($user->getId());
        return $staff;
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $this->repository->getClassName() === $class
        || is_subclass_of($class, $this->repository->getClassName());
    }
}