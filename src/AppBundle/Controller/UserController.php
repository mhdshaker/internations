<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;

use AppBundle\Services\GroupManager;
use AppBundle\Services\StaffManager;
use AppBundle\Services\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AppController
{


    /**
     * @Route("/users", name="users")
     */
    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->getResult();


        return $this->render('user/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/user/{id}", name="user-details")
     */
    public function detailsAction(Request $request, $id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);


        return $this->render('user/details.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/user/{id}/assign/{group_id}", name="assign-user-to-group")
     * @param $id
     * @param $group_id
     */
    public function assignUserToGroup($id, $group_id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);

        $group = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->find($group_id);

        $staff = $this->servicesManager->getServiceManager(StaffManager::class)->getAdmin();

        /** @var GroupManager $groupManager */
        $groupManager = $this->servicesManager->getServiceManager(GroupManager::class);
        $groupManager->assignUserToGroup($user, $group, $staff);
    }

    /**
     * @Route("/user/{id}/remove/{group_id}", name="remove-user-from-group")
     * @param $id
     * @param $group_id
     */
    public function removeUserFromGroup($id, $group_id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);

        $group = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->find($group_id);

        $staff = $this->servicesManager->getServiceManager(StaffManager::class)->getAdmin();

        /** @var GroupManager $groupManager */
        $groupManager = $this->servicesManager->getServiceManager(GroupManager::class);
        $groupManager->removeUserFromGroup($user, $group, $staff);
    }

}