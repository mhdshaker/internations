<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class GroupController extends AppController
{
    /**
     * @Route("/groups", name="groups")
     */
    public function indexAction(Request $request)
    {
        $groups = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->getResult();


        return $this->render('group/index.html.twig', [
            'groups' => $groups
        ]);
    }

    /**
     * @Route("/group/{id}", name="group-details")
     */
    public function detailsAction(Request $request, $id)
    {
        $group = $this->getDoctrine()
            ->getRepository('AppBundle:Group')
            ->find($id);


        return $this->render('group/details.html.twig', [
            'group' => $group
        ]);
    }
}