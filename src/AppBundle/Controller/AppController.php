<?php

namespace AppBundle\Controller;

use AppBundle\Services\ServicesManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AppController extends Controller
{
    /**
     * @var $servicesManager ServicesManager
     * @DI\Inject("app_bundle.services.services_manager")
     */
    protected $servicesManager;

}