function apiAssignToGroup(user_id, group_id, success_callback, fail_callback) {
    $.ajax({
        url: '/api/users/'+user_id+'/groups/'+group_id,
        type: 'POST',
        success: function(result) {
            if(success_callback) success_callback();
        },
        error:function (result) {
            if(fail_callback) fail_callback();
            alert(JSON.parse(result.responseText).message);
        }

    });
}

function apiRemoveFromGroup(user_id, group_id, success_callback, fail_callback) {
    $.ajax({
        url: '/api/users/'+user_id+'/groups/'+group_id,
        type: 'DELETE',
        success: function(result) {
            if(success_callback) success_callback();
        },
        error:function (result) {
            if(fail_callback) fail_callback();
            alert(JSON.parse(result.responseText).message);
        }

    });
}

function apiAddUser(name, success_callback, fail_callback){
    $.ajax({
        url: '/api/users',
        type: 'POST',
        data: {name: name},
        success: function(result) {
            if(success_callback) success_callback();
        },
        error:function (result) {
            if(fail_callback) fail_callback();
            alert(JSON.parse(result.responseText).message);
        }

    });
}

function apiDeleteUser(user_id, success_callback, fail_callback){
    $.ajax({
        url: '/api/users/'+user_id,
        type: 'DELETE',
        success: function(result) {
            if(success_callback) success_callback();
        },
        error:function (result) {
            if(fail_callback) fail_callback();
            alert(JSON.parse(result.responseText).message);
        }

    });
}

function apiAddGroup(name, success_callback, fail_callback){
    $.ajax({
        url: '/api/groups',
        type: 'POST',
        data: {name: name},
        success: function(result) {
            if(success_callback) success_callback();
        },
        error:function (result) {
            if(fail_callback) fail_callback();
            alert(JSON.parse(result.responseText).message);
        }

    });
}

function apiDeleteGroup(group_id, success_callback, fail_callback){
    $.ajax({
        url: '/api/groups/'+group_id,
        type: 'DELETE',
        success: function(result) {
            if(success_callback) success_callback();
        },
        error:function (result) {
            if(fail_callback) fail_callback();
            alert(JSON.parse(result.responseText).message);
        }

    });
}



function deleteGroup(ele, group_id){
    apiDeleteGroup(group_id, function (){
        $(ele).attr('onclick', '');
        $(ele).attr('href','#');
        $(ele).find('span').html('');
        $(ele).closest('tr').find('.status').html('Deactivated');
    });
}

function deleteUser(ele, user_id){
    apiDeleteUser(user_id, function (){
        $(ele).attr('onclick', '');
        $(ele).attr('href','#');
        $(ele).find('span').html('');
        $(ele).closest('tr').find('.status').html('Deactivated');
    });
}


function removeFromGroup(ele, user_id, group_id){
    apiRemoveFromGroup(user_id, group_id, function (){
        $(ele).attr('onclick', 'assignToGroup(this,'+user_id+','+group_id+')');
        $(ele).attr('href','#activate');
        $(ele).find('span').html('Activate');
        $(ele).closest('tr').find('.status').html('Deactivated');
    });
}

function assignToGroup(ele, user_id, group_id){
    apiAssignToGroup(user_id, group_id, function (){
        $(ele).attr('onclick', 'removeFromGroup(this,'+user_id+','+group_id+')');
        $(ele).attr('href','#deactivate');
        $(ele).find('span').html('Deactivate');
        $(ele).closest('tr').find('.status').html('Active');
    });
}